## Національний технічний університет України<br>“Київський політехнічний інститут ім. Ігоря Сікорського”

## Факультет прикладної математики<br>Кафедра системного програмування і спеціалізованих комп’ютерних систем


# Лабораторна робота №2<br>"Розширена робота з git"

## КВ-12 Манжелій Роман

# Хід виконання роботи

### 1. Для початку створимо папку для виконання ЛР2:
```
student@virt-linux:~$ mkdir ~/lab2
student@virt-linux:~$ cd ~/lab2
student@virt-linux:~/lab2$ 

```


### 2. Зклонуємо репозиторій для ЛР2. Використовуємо абсолютний шлях до локальної папки з репозиторієм від ЛР1:
```
student@virt-linux:~/lab2$ git clone file:///home/student/lab1/Vencord
Cloning into 'Vencord'...
remote: Enumerating objects: 10601, done.
remote: Counting objects: 100% (10601/10601), done.
remote: Compressing objects: 100% (2916/2916), done.
remote: Total 10601 (delta 7425), reused 10588 (delta 7421)
Receiving objects: 100% (10601/10601), 3.47 MiB | 13.06 MiB/s, done.
Resolving deltas: 100% (7425/7425), done.

```


### 3. Перевіримо, що в копії репозиторію ЛР2 у нас присутні гілки, які ми створювали для ЛР1:
```
student@virt-linux:~/lab2/Vencord$ git branch -a
* main
  remotes/origin/HEAD -> origin/main
  remotes/origin/main
  remotes/origin/my-local-branch-1
  remotes/origin/my-local-branch-2
  remotes/origin/my-local-branch-3

```

### 4. Перевіримо, куди "дивиться" наш поточний ремоут origin:
```
student@virt-linux:~/lab2/Vencord$ git remote -v
origin	file:///home/student/lab1/Vencord (fetch)
origin	file:///home/student/lab1/Vencord (push)
```

### 5. Додамо ще один ремоут до нашої локальної копії:
```
student@virt-linux:~/lab2/Vencord$ git remote add upstream https://gitlab.com/tirpz2/advanced-git.git
student@virt-linux:~/lab2/Vencord$ git remote -v
origin	file:///home/student/lab1/Vencord (fetch)
origin	file:///home/student/lab1/Vencord (push)
upstream	https://gitlab.com/tirpz2/advanced-git.git (fetch)
upstream	https://gitlab.com/tirpz2/advanced-git.git (push)

```

### 6. Отримаємо список змін з нового ремоута:
````
student@virt-linux:~/lab2/Vencord$ git fetch upstream
warning: no common commits
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 286 bytes | 143.00 KiB/s, done.
From https://gitlab.com/tirpz2/advanced-git
 * [new branch]      main       -> upstream/main


student@virt-linux:~/lab2/Vencord$ git branch -a
* main
  remotes/origin/HEAD -> origin/main
  remotes/origin/main
  remotes/origin/my-local-branch-1
  remotes/origin/my-local-branch-2
  remotes/origin/my-local-branch-3
  remotes/upstream/main

````

### 7. Переключимося на гілку remotes/upstream/main і покажемо, що тепер у нашій директорії з'явився новий файл, що лежав на віддаленому репозиторії
````
student@virt-linux:~/lab2/Vencord$ git checkout remotes/upstream/main
student@virt-linux:~/lab2/Vencord$ git ls-files
show_git_fetch_upstream_work

````

### 8. Створимо нову гілку від головної гілки, і покладемо в неї кілька комітів. І зробимо push без зв'язування гілки із віддаленим репозиторієм (Директорія ЛР1)
````
$ git checkout -b lab2-branch
. . .
# додали кілька комітів в нову гілку
. . .
student@virt-linux:~/lab2/Vencord$ git push origin lab2-branch
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 2 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (6/6), 553 bytes | 61.00 KiB/s, done.
Total 6 (delta 2), reused 0 (delta 0)
To file:///home/student/lab1/Vencord
 * [new branch]      lab2-branch -> lab2-branch
````



### 9. Зробимо ще коміт і пушнемо вже зв'язавши гілку з репозиторієм
````
student@virt-linux:~/lab2/Vencord$ git push -u origin lab2-branch
Branch 'lab2-branch' set up to track remote branch 'lab2-branch' from 'origin'.
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 2 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 288 bytes | 288.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To file:///home/student/lab1/Vencord
   d39ac58..a8c937e  lab2-branch -> lab2-branch
````

###  10. Додамо до гілки ще коміт. Переконаємося в тому, що після зв'язування гілок тепер можна пушити просто через git push
````
student@virt-linux:~/lab2/Vencord$ git push
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 2 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 290 bytes | 290.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To file:///home/student/lab1/Vencord
   a8c937e..8fcfe0c  lab2-branch -> lab2-branch

````

###  11. Перевіримо в репозиторії ЛР1, що після пушу тут з'явилася нова локальна гілка (яку ми власне пушнули).
````
student@virt-linux:~/lab1/Vencord$ git branch -a
* lab2-branch
  main
  my-local-branch-1
  my-local-branch-2
  my-local-branch-3
  remotes/origin/HEAD -> origin/main
  remotes/origin/SwcParser
  remotes/origin/componentUpdaterAPI
  remotes/origin/dev
  remotes/origin/fakenitro-unexplode
  remotes/origin/feat/permissionfreewill-onboarding-delete
  remotes/origin/feat/relationship-notifier
  remotes/origin/feat/startup-timings-tti
  remotes/origin/features/component-library
  remotes/origin/main
  remotes/origin/npm-types
  remotes/origin/serverProfile

````

###  12. Мердж гілок
````
student@virt-linux:~/lab2/Vencord$ git merge origin/my-local-branch-1
Already up to date.
````


###  13. Ми створили три файли 6, 7, 8 на гілці lab2-branch-2 . По одному на комміт. Використовуючи cherry-pick перенесли середній комміт (файл 7) до гілки lab2-branch
````
student@virt-linux:~/lab2/Vencord$ git checkout lab2-branch
Switched to branch 'lab2-branch'
Your branch is up to date with 'origin/lab2-branch'.
student@virt-linux:~/lab2/Vencord$ git cherry-pick 19b4fa1faa9b64d239d4b9e923c0194b22098b20
[lab2-branch 606a4e7] file7.txt
 Date: Fri Dec 22 04:18:37 2023 +0200
 1 file changed, 1 insertion(+)
 create mode 100644 file7.txt


student@virt-linux:~/lab2/Vencord$ git log --pretty=oneline --graph -n 10 --branches
* 606a4e7aec22b4ecf5d23e8442173ec20bdfb569 (HEAD -> lab2-branch) file7.txt
* 8fcfe0cd54059a0ebea103bf91cec10fdfc57ee9 (origin/lab2-branch) updated file5.txt
* a8c937e83896daedabb73cc4fb673b15d7e6252f file5.txt
* d39ac585fd3575c4153aa359acc75f7115168ebc updated file4.txt
* 252ffd53888fef61f7ca50c04b12d2f987f0210f file4.txt
| * 298a3e660f8ed88b846bc316acd78703c7b6ef38 (lab2-branch-2) file8.txt
| * 19b4fa1faa9b64d239d4b9e923c0194b22098b20 file7.txt
| * 412d279c8e9cf9295399708d60d1747fbdd4e615 file6.txt
|/  
* b6728125e2901b9f72d8738a2a2430a46cf70b06 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD, main) file2.txt -> renamed_file2.txt
* 71ea9d7ea5fe2ccc0afacb800bdaa32bafb67882 Deleted file1.txt

````

###  14. Визначимо останнього спільного предка між гілками
````
student@virt-linux:~/lab2/Vencord$  git merge-base lab2-branch lab2-branch-2
b6728125e2901b9f72d8738a2a2430a46cf70b06

````

###  15. Зробили трохи unstaged змін, зберегли до нички (git stash). Зробили ще трохи unstaged змін, зберегли до нички (git stash)
````
student@virt-linux:~/lab2/Vencord$ git stash list
stash@{0}: WIP on main: b672812 file2.txt -> renamed_file2.txt
stash@{1}: WIP on main: b672812 file2.txt -> renamed_file2.txt

````

###  16. Дістали з нички перші збережені зміни
````
student@virt-linux:~/lab2/Vencord$ git stash apply stash@{1}
````

###  17. Створили кілька файлів з унікальним розширенням kv12
````
student@virt-linux:~/lab2/Vencord$ git status
On branch main
Your branch is up to date with 'origin/main'.

Untracked files:
(use "git add <file>..." to include in what will be committed)
first.kv12
second.kv12
test.txt

nothing added to commit but untracked files present (use "git add" to track)
````


###  18. Додаkи шаблон для цих файлів в ігнор. Перевірити статус -- файли зникли
````
student@virt-linux:~/lab2/Vencord$ git status
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   .gitignore

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	test.txt
````

###  19. Перевірили статус включно з ігнором.
````
student@virt-linux:~/lab2/Vencord$ git status --ignored
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   .gitignore

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	test.txt

Ignored files:
  (use "git add -f <file>..." to include in what will be committed)
	first.kv12
	second.kv12
````

###  20. Почистили всі untracked файли з репозиторію, включно з ігнорованими.
````
student@virt-linux:~/lab2/Vencord$  git clean -fdx
Removing first.kv12
Removing second.kv12
Removing test.txt
````

###  21. Переглянули лог.
````
student@virt-linux:~/lab2/Vencord$  git log --pretty=oneline --graph -n 15
* 606a4e7aec22b4ecf5d23e8442173ec20bdfb569 (HEAD -> lab2-branch) file7.txt
* 8fcfe0cd54059a0ebea103bf91cec10fdfc57ee9 (origin/lab2-branch) updated file5.txt
* a8c937e83896daedabb73cc4fb673b15d7e6252f file5.txt
* d39ac585fd3575c4153aa359acc75f7115168ebc updated file4.txt
* 252ffd53888fef61f7ca50c04b12d2f987f0210f file4.txt
* b6728125e2901b9f72d8738a2a2430a46cf70b06 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) file2.txt -> renamed_file2.txt
* 71ea9d7ea5fe2ccc0afacb800bdaa32bafb67882 Deleted file1.txt
* c766bedda179c41c79871547be29722901ed1606 new file1.txt
* 64c6f5740fa389a7672bd53c863d6f4549996907 Fix FakeNitro completely (#1903)
* 03523446c177aa7a71b75a6c9b4eba2e9e04c963 silentTyping: fix showIcon toggle (#1898)
* 25dc25c707fe6582a7aea1f5c5ccf68ef0b48987 Fix VoiceMessages
* 8ac80488454ba789791f69eb1e196adcf3e8be3a fix: ImageZoom + PiP (#1894)
* ffe6bb1c5d4191cad35a1bdcb84695e886ff4528 (tag: v1.6.0) Bump to v1.6.0
* 74faaa216f0f8d29216db8c351ef2f883ae9fc67 Fix PlatformIndicators colorMobileIndicator
* a6b8b59d5c2be3f8894e1287479d8546bc102339 fix: shikicodeblocks, betterroledot (#1895)
````

###  22. Видалили гілку.
````
student@virt-linux:~/lab2/Vencord$ git checkout main
Switched to branch 'main'
Your branch is ahead of 'origin/main' by 1 commit.
(use "git push" to publish your local commits)
student@virt-linux:~/lab2/Vencord$ git branch -D lab2-branch
Deleted branch lab2-branch (was 606a4e7).
student@virt-linux:~/lab2/Vencord$ git push -d origin lab2-branch
To file:///home/student/lab1/Vencord
- [deleted]         lab2-branch
````

###  23. Переглянули рефлог.
````
student@virt-linux:~/lab2/Vencord$ git reflog
a276b6b (HEAD -> main) HEAD@{0}: checkout: moving from lab2-branch to main
606a4e7 HEAD@{1}: checkout: moving from main to lab2-branch
a276b6b (HEAD -> main) HEAD@{2}: commit: change gitignore
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{3}: reset: moving to HEAD
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{4}: reset: moving to HEAD
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{5}: reset: moving to HEAD
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{6}: checkout: moving from lab2-branch to main
606a4e7 HEAD@{7}: cherry-pick: file7.txt
8fcfe0c HEAD@{8}: checkout: moving from lab2-branch-2 to lab2-branch
298a3e6 (lab2-branch-2) HEAD@{9}: checkout: moving from main to lab2-branch-2
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{10}: checkout: moving from lab2-branch-2 to main
298a3e6 (lab2-branch-2) HEAD@{11}: commit: file8.txt
19b4fa1 HEAD@{12}: commit: file7.txt
412d279 HEAD@{13}: reset: moving to HEAD~1
fa74973 HEAD@{14}: commit: file7.txt
412d279 HEAD@{15}: commit: file6.txt
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{16}: checkout: moving from main to lab2-branch-2
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{17}: checkout: moving from lab2-branch to main
8fcfe0c HEAD@{18}: checkout: moving from main to lab2-branch
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{19}: checkout: moving from lab2-branch-2 to main
8fcfe0c HEAD@{20}: checkout: moving from main to lab2-branch-2
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{21}: checkout: moving from lab2-branch-2 to main
8fcfe0c HEAD@{22}: checkout: moving from lab2-branch to lab2-branch-2
8fcfe0c HEAD@{23}: commit: updated file5.txt
a8c937e HEAD@{24}: commit: file5.txt
d39ac58 HEAD@{25}: commit: updated file4.txt
252ffd5 HEAD@{26}: checkout: moving from main to lab2-branch
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{27}: checkout: moving from lab2-branch to main
252ffd5 HEAD@{28}: commit: file4.txt
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{29}: checkout: moving from main to lab2-branch
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{30}: checkout: moving from 7dc14be9984a1f914c2e0d406f8d10cd7e89a4f5 to main
7dc14be (upstream/main) HEAD@{31}: checkout: moving from main to remotes/upstream/main
b672812 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) HEAD@{32}: clone: from file:///home/student/lab1/Vencord
````


###  24. Відновили гілку.
````
student@virt-linux:~/lab2/Vencord$  git branch lab2-resurrected 606a4e7
student@virt-linux:~/lab2/Vencord$ git checkout lab2-resurrected
Switched to branch 'lab2-resurrected'
student@virt-linux:~/lab2/Vencord$  git log --pretty=oneline --graph -n 15
* 606a4e7aec22b4ecf5d23e8442173ec20bdfb569 (HEAD -> lab2-resurrected) file7.txt
* 8fcfe0cd54059a0ebea103bf91cec10fdfc57ee9 updated file5.txt
* a8c937e83896daedabb73cc4fb673b15d7e6252f file5.txt
* d39ac585fd3575c4153aa359acc75f7115168ebc updated file4.txt
* 252ffd53888fef61f7ca50c04b12d2f987f0210f file4.txt
* b6728125e2901b9f72d8738a2a2430a46cf70b06 (origin/my-local-branch-3, origin/my-local-branch-2, origin/my-local-branch-1, origin/main, origin/HEAD) file2.txt -> renamed_file2.txt
* 71ea9d7ea5fe2ccc0afacb800bdaa32bafb67882 Deleted file1.txt
* c766bedda179c41c79871547be29722901ed1606 new file1.txt
* 64c6f5740fa389a7672bd53c863d6f4549996907 Fix FakeNitro completely (#1903)
* 03523446c177aa7a71b75a6c9b4eba2e9e04c963 silentTyping: fix showIcon toggle (#1898)
* 25dc25c707fe6582a7aea1f5c5ccf68ef0b48987 Fix VoiceMessages
* 8ac80488454ba789791f69eb1e196adcf3e8be3a fix: ImageZoom + PiP (#1894)
* ffe6bb1c5d4191cad35a1bdcb84695e886ff4528 (tag: v1.6.0) Bump to v1.6.0
* 74faaa216f0f8d29216db8c351ef2f883ae9fc67 Fix PlatformIndicators colorMobileIndicator
* a6b8b59d5c2be3f8894e1287479d8546bc102339 fix: shikicodeblocks, betterroledot (#1895)
````
